# Eurooms Roomfinder Plug-in

A lightweight, HTML & CSS only room search form linking to Eurooms Roomfinder (https://www.eurooms.com/roomfinder).

## Installation

Embed the contents of the *roomfinder.html* file within the designated area of your web site or application, and add a link to the minified CSS file *css/roomfinder.min.css* in your HTML document. 

For Italian or Spanish versions use the snippets from *roomfinder-it.html* and *roomfinder-es.html*, respectively. These are exact copies of the original *roomfinder.html* file except the language content.
 
## Agent / Affiliate ID

If you are cooperating with Eurooms as an agent, you must uncomment the hidden input field with name attribute *agent_id*, and specify your agent ID as the value of this input field (see source HTML file). This is necessary to track bookings at Eurooms website which originated from your website.
 
## jQuery datepicker

The Check-in text input field of the Roomfinder accepts a date value in DD/MM/YYYY format. To enhance user experience you may use a jQuery datepicker on this field. Instructions on how to use jQuery and its plugins are beyond the scope of this document. Eurooms CSS styling for jQuery datepicker calendar widget can be provided on request.

## Example

See *example.html* to see how the Roomfinder form as well as additional jQuery datepicker can be integrated with your website.